#!/usr/bin/bash

export fixPath=/20221111_fix-main
echo "Running fix script.."

# backup
echo "Creating configuration backup.."
cp -v /etc/nginx/proxy_params{,.bkp}
cp -v /etc/init.d/box{,.bkp}

# fix nginx configuration
echo "Fixing nginx configuration.."
sed -i 's/proxy_ssl_verify on/proxy_ssl_verify off/g' /etc/nginx/proxy_params

# fix application environment
echo "Fixing application configuration.."
sed -i 's/-Xmx50m/-Xmx150m/g' /etc/init.d/box

# prepare postgres
echo "Starting postgres.."
service postgresql start

# set correct user password
echo "Prepairing createUserAndDB.sql.."
sed -i "s/dbPassword/$(cat /etc/box.properties | grep dbPassword | awk '{print $3}')/g" $fixPath/createUserAndDB.sql

# create new user and database
echo "Running createUserAndDB.sql.."
su - postgres -c "psql -U postgres -d postgres -a -f $fixPath/createUserAndDB.sql" $> /dev/null

# update database
echo "Running update.sql.."
su - postgres -c "psql -U postgres -d box -a -f $fixPath/update.sql" $> /dev/null

# deduplicate postgres start 
echo "Disable postgresql start in main script.."
sed -i "s/service postgresql start/#service postgresql start/g" /startup

# remove archive 
echo "Removing fix archive.." 
rm $fixPath.tar || true

# run main script
echo "Running main script.."
/startup 

